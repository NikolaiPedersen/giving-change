/**
 * The program will calculate and display change given in a shop
 * @since 21.09.2021
 * @author Nikolai
 * @version 0.0.1
 */
public class Main {
    public static void main(String[] args) {

        double price = 2.95;
        int dollars = (int)2.95;
        int cents = (int)((price - dollars) * 100 + 0.5);

        System.out.println(cents);
    }
}
